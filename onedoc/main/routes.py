
from flask import render_template, Blueprint, url_for
from werkzeug.utils import redirect
from onedoc.models import Document
from flask_login import current_user

main = Blueprint('main', __name__)


@main.route("/")
@main.route("/home")
def home():
    if current_user.is_anonymous == False:
        documents = Document.query.all()
        return render_template('home.html',  documents=documents)
    else:
        return redirect(url_for('users.login'))
    

@main.route("/about")
def about():
    return render_template('about.html', title='About')

