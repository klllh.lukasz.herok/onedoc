import os

class Config:
    SECRET_KEY = os.environ.get('ONEDOC_SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.environ.get('ONEDOC_DB_URI')
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'noreplybotreset@gmail.com'
    MAIL_PASSWORD = 'noreply123'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # ONEDOC_SECRET_KEY
    # ONEDOC_DB_URI