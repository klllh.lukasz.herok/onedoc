from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
from onedoc.config import Config
from flask_migrate import Migrate



db = SQLAlchemy()
bcrypt = Bcrypt()
login_manager = LoginManager()
login_manager.login_view = 'users.login'
login_manager.login_message_category = 'info'
migrate = Migrate(compare_type = True)

mail = Mail()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)
    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)
    migrate.init_app(app, db)

    from onedoc.users.routes import users
    from onedoc.sections.routes import sections
    from onedoc.main.routes import main
    from onedoc.documents.routes import documents
    from onedoc.branches.routes import branches

    app.register_blueprint(users)
    app.register_blueprint(sections)
    app.register_blueprint(main)
    app.register_blueprint(documents)
    app.register_blueprint(branches)

    return app