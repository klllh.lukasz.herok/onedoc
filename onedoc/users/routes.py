from flask import render_template, url_for, flash, redirect, request, Blueprint, abort
from flask_login import login_user, current_user, logout_user, login_required
from onedoc import db, bcrypt
from onedoc.models import User, Section, Branch
from onedoc.users.forms import (RegistrationForm, LoginForm, UpdateAccountForm,
                                   RequestResetForm, ResetPasswordForm, UpdateAccountFormUser)
from onedoc.users.utils import save_picture, send_reset_email

users = Blueprint('users', __name__)


@users.route("/register", methods=['GET', 'POST'])
@login_required
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password, role=form.role.data, id_branch=form.branch.data)
        db.session.add(user)
        db.session.commit()
        flash('Konto założone pomyślnie!', 'success')
        return redirect(url_for('users.login'))
    return render_template('register.html', title='Register', form=form)


@users.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('main.home'))
        else:
            flash('Logowanie nieudane, sprawdź wprowadzone dane', 'danger')
    return render_template('login.html', title='Login', form=form)


@users.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('main.home'))
    

@users.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    branches = Branch.query.all()
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Konto zostało zaktualizowane', 'success')
        return redirect(url_for('users.account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename='profile_pics/' + current_user.image_file)
    return render_template('account.html', title='Account',
                           image_file=image_file, form=form, branches=branches)


@users.route("/user/<string:username>")
def sections(username):
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(username=username).first_or_404()
    sections = Section.query.filter_by(author=user)\
        .order_by(Section.date_posted.desc())\
        .paginate(page=page, per_page=5)
    return render_template('user_sections.html', sections=sections, user=user)

@users.route("/res_pass", methods=['GET', 'POST'])
@login_required
def res_pass():
    form = ResetPasswordForm()
    if form.validate_on_submit():
        current_user.password = form.password.data
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        current_user.password = hashed_password
        db.session.commit()
        flash('Hasło zostało zmienione', 'success')
        return redirect(url_for('users.account'))
    return render_template('res_pass.html', title='Account', form=form)

@users.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash('E-mail wraz z instrukcjami do resetu hasła został pomyślnie wysłany na podany adres.', 'info')
        return redirect(url_for('users.login'))
    return render_template('reset_request.html', title='Reset Password', form=form)


@users.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    user = User.verify_reset_token(token)
    if user is None:
        flash('Nieprawidłowy lub wygasły token. Spróbuj jeszcze raz.', 'warning')
        return redirect(url_for('users.reset_request'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash('Twoje hasło zostało pomyślnie zmienione!', 'success')
        return redirect(url_for('users.login'))
    return render_template('reset_token.html', title='Reset Password', form=form)

@users.route("/users_list")
@login_required
def list():
    users = User.query.all()
    if current_user.role != "admin":
        return redirect(url_for('main.home'))
    else:
        return render_template('users_list.html', users=users)

@users.route("/admin_account/<int:id_user>", methods=['GET', 'POST'])
@login_required
def admin_account(id_user):
    userbranch = request.form.get('selectbranch')
    print(userbranch)
    branches = Branch.query.all()
    if current_user.role != 'admin':
        return redirect(url_for('main.home'))
    user = User.query.get_or_404(id_user)
    form = UpdateAccountFormUser()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            user.image_file = picture_file
        user.username = form.username.data
        user.email = form.email.data
        user.id_branch = userbranch
        db.session.commit()
        flash('Konto zostało zaktualizowane', 'success')
        return redirect(url_for('users.admin_account', id_user=user.id))
    elif request.method == 'GET':
        form.username.data = user.username
        form.email.data = user.email
    image_file = url_for('static', filename='profile_pics/' + user.image_file)
    return render_template('admin_account.html', title='Account',
                           image_file=image_file, form=form, user=user, branches=branches)

# Delete of the user in the List screen
@users.route("/admin_account/<int:id_user>/delete", methods=['POST'])
@login_required
def delete(id_user):
    user = User.query.get_or_404(id_user)
    if current_user.role != 'admin':
        abort(403)
    db.session.delete(user)
    db.session.commit()
    flash('Użytkownik został usunięty!', 'warning')
    return redirect(url_for('users.list'))

