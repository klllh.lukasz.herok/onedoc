// const btn = document.querySelector("#button")

// function change() {
//     //changeAttr()
//     document.body.classList.toggle('dark-mode');
//     if(document.body.classList.contains('dark-mode')) {
//        btn.value = "👓"
//     }else {
//         btn.value = "🕶";
//     }
// }

// btn.addEventListener('click', change);

function stringIntoChunks(str, chunkSize) {
    const numChunks = Math.ceil(str.length / chunkSize);
    const chunks = new Array(numChunks);
  
    for (let i = 0, o = 0; i < numChunks; ++i, o += chunkSize) {
        chunks[i] = str.substr(o, chunkSize);
    }
  
    return chunks;
}

function fileSelected(fileInput, picElementId)
{
    let file = fileInput.files[0];
    let reader = new FileReader();
    reader.readAsBinaryString(file);

    reader.onload = () => {
        let fileUploadData = {
            "action" : "uploadFile",
            "filename" : file.name,
            "content"  : btoa(reader.result)
        };
        console.log(fileUploadData);
        $.ajax({
            url : "",
            type: "POST",
            data: fileUploadData,
            success: (uploadedFilePath) => {
                $("#" + picElementId).attr("src", uploadedFilePath);
            },
            error: (data) => { alert("Error"); console.log(data); } 
        });
    };
}
