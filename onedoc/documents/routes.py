from flask import (render_template, url_for, flash,
                   redirect, Blueprint, abort, request)
from flask_login import current_user, login_required
from onedoc import db
from onedoc.models import Document, Branch, Section
from onedoc.documents.forms import DocForm, UpdateDocForm
from markupsafe import Markup
import markdown

documents = Blueprint('documents', __name__)


@documents.route("/create_document", methods=['GET', 'POST'])
@login_required
def new():
    if current_user.role != 'admin':
        return redirect(url_for('main.home'))
    form = DocForm()
    if form.validate_on_submit():
        document = Document(title=form.title.data)
        db.session.add(document)
        db.session.commit()
        flash('Dokument został utworzony!', 'success')
        return redirect(url_for('main.home'))
    return render_template('create_document.html', title='New Document',
                           form=form, legend='Nowy dokument')


@documents.route("/documents/<int:id_document>")
def show(id_document):
    document = Document.query.get_or_404(id_document)
    user_sections = []


    if current_user.is_anonymous:
        return redirect(url_for('main.home'))

    sec_def = list(filter(lambda sec: sec.id_branch is None, document.sections))
    for s in sec_def:
        try:
            # Check if there is a dedicated section for user branch
            sec_b = next(filter(lambda sec: sec.id_branch == current_user.id_branch and sec.id_section_default == s.id,
                                document.sections))
            user_sections.append(sec_b)
        except StopIteration:
            user_sections.append(s)
    
    l = []

    for i in range(len(user_sections)):
        l.append(i+1)
    
    last_btn = len(l) + 1

    markdown_content = []

    for user_section in user_sections:
        body = Markup(markdown.markdown(user_section.content, extensions=['fenced_code', 'footnotes', 'toc']))
        markdown_content.append(body)

    return render_template('document.html', document=document, last_btn=last_btn, sections_l = zip(user_sections, l, markdown_content))

 
@documents.route("/documents/<int:id_document>/update", methods=['GET', 'POST'])
@login_required
def update(id_document):
    document = Document.query.get_or_404(id_document)
    if current_user.role != 'admin':
        abort(403)
    form = UpdateDocForm()
    if form.validate_on_submit():
        document.title = form.title.data
        db.session.commit()
        flash('Dokument został zaktualizowany!', 'success')
        return redirect(url_for('main.home'))
    elif request.method == 'GET':
        form.title.data = document.title
    return render_template('create_document.html', title='Update Document',
                           form=form, legend='Edycja dokumentu')


@documents.route("/documents/<int:id_document>/delete", methods=['GET', 'POST'])
@login_required
def delete(id_document):
    document = Document.query.get_or_404(id_document)
    if current_user.role != 'admin':
        abort(403)
    db.session.delete(document)
    db.session.commit()
    flash('Dokument został usunięty!', 'success')
    return redirect(url_for('main.home'))
