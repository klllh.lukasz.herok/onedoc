from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, ValidationError
from onedoc.models import Document

class DocForm(FlaskForm):
    title = StringField('Tytuł', validators=[DataRequired()])
    submit = SubmitField('Utwórz dokument')


class UpdateDocForm(FlaskForm):
    title = StringField('Tytuł', validators=[DataRequired()])
    submit = SubmitField('Zapisz')

    def validate_title(self, title):
        title = Document.query.filter_by(title=title.data).first()
        if title:
            raise ValidationError('Ta nazwa dokumentu jest zajęta, proszę wybrać inną.')