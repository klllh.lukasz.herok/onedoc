from datetime import datetime
from enum import unique
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from onedoc import db, login_manager
from flask_login import UserMixin
from onedoc import __init__

@login_manager.user_loader
def load_user(id_user):
    return User.query.get(int(id_user))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default='default.png')
    password = db.Column(db.String(60), nullable=False)
    sections = db.relationship('Section', backref='author', lazy=True)
    role = db.Column(db.String(20), unique=False, nullable=True)
    id_branch = db.Column(db.Integer, db.ForeignKey('branch.id'), nullable=False)

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(current_app.config['ONEDOC_SECRET_KEY'], expires_sec)
        return s.dumps({'id_user': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config['ONEDOC_SECRET_KEY'])
        try:
            id_user = s.loads(token)['id_user']
        except:
            return None
        return User.query.get(id_user)

    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.image_file}')"


class Section(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    content = db.Column(db.Text, nullable=False)
    document_sort = db.Column(db.Integer)
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    id_document = db.Column(db.Integer, db.ForeignKey('document.id'), nullable=False)
    id_section_default = db.Column(db.Integer, db.ForeignKey('branch.id'), nullable=True)
    id_branch = db.Column(db.Integer, nullable=True)
    
    def __repr__(self):
        return f"Post('{self.title}', '{self.date_posted}', '{self.document_sort}')"


class Document(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    title = db.Column(db.String(35), nullable=False)
    sections = db.relationship('Section', lazy=True, order_by=(Section.document_sort).asc())


class Branch(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False, unique=True)
    name = db.Column(db.String(10), nullable=False)
    users = db.relationship('User', lazy=True)
    sections = db.relationship('Section', lazy=True)

    
    



