from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, ValidationError
from onedoc.models import Branch

class BranchForm(FlaskForm):
    name = StringField('Nazwa', validators=[DataRequired()])
    submit = SubmitField('Utwórz gałąź')

    def validate_name(self, name):
            name = Branch.query.filter_by(name=name.data).first()
            if name:
                raise ValidationError('Ta nazwa jest zajęta, proszę wybrać inną')