from flask import (render_template, url_for, flash,
                   redirect, request, Blueprint, abort)
from flask_login import current_user, login_required
from onedoc import db
from onedoc.models import Branch
from onedoc.branches.forms import BranchForm

branches = Blueprint('branches', __name__)

@branches.route("/create_branch", methods=['GET', 'POST'])
@login_required
def new():
    if current_user.role != 'admin':
         return redirect(url_for('main.home'))
    form = BranchForm()
    if form.validate_on_submit():
        branch = Branch(name=form.name.data)
        db.session.add(branch)
        db.session.commit()
        flash('Gałąź została utworzona!', 'success')
        return redirect(url_for('branches.list'))
    return render_template('create_branch.html', 
                           form=form, legend='Nowa gałąź')

@branches.route("/branches_list")
@login_required
def list():
    branches = Branch.query.all()
    if current_user.role != "admin":
        return redirect(url_for('main.home'))
    else:
        return render_template('branches_list.html', branches=branches)


@branches.route("/branches_list/<int:id_branch>/update", methods=['GET', 'POST'])
@login_required
def update(id_branch):
    branch = Branch.query.get_or_404(id_branch)
    if current_user.role != 'admin':
        abort(403)
    form = BranchForm()
    if form.validate_on_submit():
        branch.name = form.name.data
        db.session.commit()
        flash('Gałąź została utworzona!', 'success')
        return redirect(url_for('branches.list', id_branch=branch.id))
    elif request.method == 'GET':
        form.name.data = branch.name
    return render_template('create_branch.html', title='Update Branch',
                           form=form, legend='Edycja gałęzi')

@branches.route("/branches_list/<int:id_branch>/delete", methods=['POST'])
@login_required
def delete(id_branch):
    branch = Branch.query.get_or_404(id_branch)
    if current_user.role != 'admin':
        abort(403)
    db.session.delete(branch)
    db.session.commit()
    flash('Gałąź została usunięta!', 'success')
    return redirect(url_for('branches.list'))