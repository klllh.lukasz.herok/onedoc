from flask import (render_template, url_for, flash,
                   redirect, Blueprint)
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, SelectField, FileField
from wtforms.fields.core import IntegerField
from wtforms.validators import DataRequired
from wtforms.widgets.core import Input
from onedoc.models import Document, Section, Branch


class SectionForm(FlaskForm):
    title = StringField('Tytuł')
    content = TextAreaField('Treść')
    submit = SubmitField('Zapisz')


class UpdateSectionForm(FlaskForm):
    title = StringField('Tytuł')
    content = TextAreaField('Treść')
    submit = SubmitField('Zapisz')


class ConfigForm(FlaskForm):
    title = StringField('Tytuł')
    content = TextAreaField('Treść')

    branch = SelectField('Wybierz gałąź: ')
    def __init__(self):
        super(ConfigForm, self).__init__()
        self.branch.choices = [(c.id, c.name) for c in Branch.query.all()]
    submit = SubmitField('Zapisz')