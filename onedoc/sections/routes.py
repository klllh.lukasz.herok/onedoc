from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint)
from flask_login import current_user, login_required
from onedoc import db
from onedoc.models import Section, Branch, Document
from onedoc.sections.forms import SectionForm, UpdateSectionForm, ConfigForm
from markupsafe import Markup
import markdown


sections = Blueprint('sections', __name__)


@sections.route("/section/new/<int:doc_id>/<int:btn_num>", methods=['GET', 'POST'])
@login_required
def new(doc_id, btn_num):
    document = Document.query.get_or_404(doc_id)

    if current_user.role != 'admin' and current_user.role != 'moderator':
         return redirect(url_for('main.home'))
    form = SectionForm()

    #sorting of sections
    
    current_doc_sec = list(filter(lambda sec: sec.id_document == doc_id, document.sections))
    
    if current_doc_sec:
        next_sec = list(filter(lambda sec: sec.document_sort >= btn_num, document.sections))
        
        for s in next_sec:
            s.document_sort = s.document_sort+1
    else:
        btn_num = 1
        

    if form.validate_on_submit():
        section = Section(title=form.title.data, id_document=doc_id, content=form.content.data, author=current_user, document_sort=btn_num)
        db.session.add(section)
        db.session.commit()
        flash('Sekcja została utworzona!', 'success')
        return redirect(url_for('documents.show', id_document = section.id_document))
    return render_template('create_section.html', title='New Section',
                           form=form, legend='Nowa Sekcja')


@sections.route("/section/<int:section_id>")
def show(section_id):
    branches = Branch.query.all()
    section = Section.query.get_or_404(section_id)
    body = Markup(markdown.markdown(section.content, extensions=['fenced_code', 'footnotes', 'toc']))
    return render_template('section.html', title=section.title, branches=branches, section=section, body=body)


@sections.route("/branch_section/<int:id_branch>")
def show_branch(id_branch):
    sections = Section.query.all()
    section = Section.query.get_or_404(id_branch)
    return render_template('branch_section.html', sections=sections, section=section, id_branch=id_branch)


@sections.route("/section/<int:section_id>/update", methods=['GET', 'POST'])
@login_required
def update(section_id):
    section = Section.query.get_or_404(section_id)
    documents = Document.query.all()
    if current_user.role != 'admin' and current_user.role != 'moderator':
        abort(403)
    form = UpdateSectionForm()
    if form.validate_on_submit():
        section.title = form.title.data
        section.content = form.content.data
        db.session.commit()
        flash('Twoja sekcja została zaktualizowana!', 'success')
        return redirect(url_for('documents.show', id_document = section.id_document))
    elif request.method == 'GET':
        form.title.data = section.title
        form.content.data = section.content
    return render_template('update_section.html', title='Update Section',
                           form=form, legend='Edycja sekcji', documents=documents, section=section)


@sections.route("/section/<int:section_id>/delete", methods=['POST'])
@login_required
def delete(section_id):
    if current_user.role != 'admin' and current_user.role != 'moderator':
        abort(403)
    
    section = Section.query.get_or_404(section_id)
    document = Document.query.get_or_404(section.id_document)
    
    sec = list(filter(lambda sec: sec.document_sort > section.document_sort, document.sections))
    
    if sec:
        for s in sec:
            s.document_sort = s.document_sort-1

    db.session.delete(section)
    db.session.commit()
    flash('Twoja sekcja została usunięta!', 'success')
    return redirect(url_for('main.home'))

@sections.route("/section/<int:section_id>/config", methods=['GET', 'POST'])
@login_required
def add_for_branch(section_id):
    section = Section.query.get_or_404(section_id)
    branches = Branch.query.all()
    if current_user.role != 'admin':
        abort(403)
    form = ConfigForm()
    if form.validate_on_submit():
        conf_section = Section(title=form.title.data, id_document=section.id_document, content=form.content.data, author=current_user, id_section_default = section_id, id_branch=form.branch.data, document_sort=section.document_sort)
        db.session.add(conf_section)
        db.session.commit()
        flash('Wydzielono gałąź', 'success')
        return redirect(url_for('sections.show', section_id=section.id))
    elif request.method == 'GET':
        form.title.data = section.title
        form.content.data = section.content
    return render_template('add_for_branch.html', title='Add For Branch',
                           form=form, legend='Wydzielanie dla gałęzi',
                           branches=branches)

@sections.route("/section/<int:doc_id>/<int:doc_sort>/up", methods=['GET', 'POST'])
@login_required
def up(doc_id, doc_sort):
    document = Document.query.get_or_404(doc_id)
    sec = next(filter(lambda sec: sec.document_sort==doc_sort and sec.id_document == doc_id, document.sections))
    sec_upper = next(filter(lambda sec: sec.document_sort==doc_sort-1 and sec.id_document == doc_id, document.sections))

    sec.document_sort = sec.document_sort-1
    sec_upper.document_sort = sec.document_sort+1

    db.session.commit()

    return redirect(url_for('documents.show', id_document = doc_id))


@sections.route("/section/<int:doc_id>/<int:doc_sort>/down", methods=['GET', 'POST'])
@login_required
def down(doc_id, doc_sort):
    document = Document.query.get_or_404(doc_id)
    sec = next(filter(lambda sec: sec.document_sort==doc_sort and sec.id_document == doc_id, document.sections))
    sec_lower = next(filter(lambda sec: sec.document_sort==doc_sort+1 and sec.id_document == doc_id, document.sections))

    sec.document_sort = sec.document_sort+1
    sec_lower.document_sort = sec.document_sort-1

    db.session.commit()

    return redirect(url_for('documents.show', id_document = doc_id))
